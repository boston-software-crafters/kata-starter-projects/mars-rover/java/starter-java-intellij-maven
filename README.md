# Kata Starter Project for Java using IntelliJ and Maven

## Prerequisites

To be successful with this kata, you, the developer(s), should be prepared to take control of a computer running an instance of IntelliJ IDEA (Community Edition or better).

The facilitator has set up Zoom (or equivalent) so that you can have this control as the current "driver" when it is your turn.
 
When you are assigned control of the mouse and keyboard the first thing you want to do is to ensure that your keymap is appropriate for your computer operating system (OS).

 | OS | Keymap |
 |----|--------|
 |Windows | Visual Studio or IntelliJ IDEA Classic |
 |Mac | macos |
 |Linux | GNOME or KDE or XWin |

You should be familiar with Git and the IntelliJ IDEA Git menu and/or shortcuts.

Basic understanding of GitHub or GitLab flow and an account on GitLab is also required.

If you feel weak in any of these areas, reach out to one of the facilitators or the organizer for some help.

### Developing a solution with TDD

The facilitator has cloned and configured the kata in **IntelliJ**. You can begin your work of solving the kata using TDD.

As you progress you can decide if and when to push your work.

It is strongly recommended that you push your final work if only to allow for working on the kata after the meetup event.

Once you, the developer(s) are ready to start, do the following:

1) Verify the environment is ready by clicking on the green arrow in the file `solution/src/test/java/org/bsc/katas/MainTest.java` on line 8.

This test should pass verifying that the solution environment is ready.

2) Add your first real test using the pattern from the test named `verify the development environment works`

3) Each time you write enough test code to eliminate all compilation errors, re-run all the tests in the `MainTest` class.

For any and all tests that fail, fix the production solution in the file `solution/src/main/java/org/bsc/katas/Main.java`.

4) Run the code coverage tool built into **IntelliJ** by selecting the appropriate item from the run test control (green arrow).

**IntelliJ** will produce a report which provides a color coded listing for the production code. Green is good. Yellow indicates a branch not taken. Red indicates code not executed.

These yellow and red code paths reflect opportunities to add tests to improve the code coverage.  All green is the goal.

**IntelliJ** also highlights the coverage status in the editor pane gutter on a file by file basis.

## Kata Instructions

Complete instructions can be found on the [Katalyst Site](https://katalyst.codurance.com/mars-rover) which includes a video of Sandro Mancuso solving the Mars Rover Kata.

You are encouraged to watch the video, even before attempting it yourself, but this is optional. If you do so, try to take a different approach when doing your solution.

In fact, if you have the time, check out any number of solutions ahead of time. The solution is not that important. The TDD process, once you have a solution in mind, is important, as is collaborating with your team.

So if you do look at other solutions, please avoid looking at the associated tests. That defeats the whole point of the exercise. Comparing after the fact might be a good thing to do.

The first step in solving the kata is to understand the constraints. Once you think you understand them, list your assumptions. Sandro does a nice job at this near the beginning of his video.

With your basic understanding, then start devising tests that yield the production code that solves the problem. The examples on the Katalyst Site are a good choice for tests.

As you build a suite of tests, you might see patterns emerge that are good to use in your testing. Go for it.

But most of all, have some fun.

## Challenges [optional]

If you are already in the Internet Hall Of Fame and have finished this kata before everyone else has read the README.md file, then here are some challenges for you:

1) Do a functional programming style solution if you've already done an OO solution. Or vice-versa. Which solution is better? faster? 

2) Make your solution clean, as in the principles and practices of Robert "Uncle Bob" Martin and his Clean Code world: use good names, short functions, good structure, SOLID code, etc.

## Solution Notes

Developer(s) should add notes here about the solution.
